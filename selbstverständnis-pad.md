Diese Datei enthält als Referenz den kompletten Inhalt des Etherpads zum Selbsterständnis (siehe: <pad-url>p/bub-selbstverstaendnis), inklusive aller Textbausteine

Sinn dieser Seite:
    Formulierung zum Selbstverständnis von Bits und Bäume Dresden

siehe https://pad.fsfw-dresden.de/p/bub-treffen-14, https://pad.fsfw-dresden.de/p/bub-treffen-22

- sollte im wesentlichen die Fragen beantworten:
    - was machen wir?
    - warum machen wir das?
    - wie machen wir das?

---

Entwurf von Carsten (2020-05-17 00:53:38):

# Bits und Bäume Dresden Selbstverständnis

## Thema und Ausrichtung
Die Gruppe befasst sich mit der Schnittmenge der Themen *Nachhaltigkeit* und *Digitalisierung*.
Die inhaltliche Ausrichtung orientiert sich dabei an den [Forderungen der Bits-und-Bäume-Konferenz 2018](https://bits-und-baeume.org/forderungen/info/de).

## Ziele

Unsere Ziele sind:

- Die Vernetzung von digitaler Bürger:innenrechte-Bewegung und allgemein technisch interessierten Menschen ("Bits") und umweltorientierter Nachhaltigkeitsbewegung ("Bäume").
- Die Befähigung zivilgesellschaftlicher Akteur:innen und öffentlicher Institutionen zu möglichst nachhaltiger IT-Nutzung.
- Öffentlichkeitsarbeit für die Themen und Forderungen der Bits- und Bäume Konferenz.
- Motivation und Befähigung von Individuen zur Umsetzung der Forderungen beizutragen.

## Was die Gruppe bietet

Bits und Bäume Dresden ist eine Plattform, um eigene thematisch passende Ideen umzusetzen. Dafür bietet die Gruppe eine organisatorischen und sozialen Rahmen, d.h. regelmäßige Treffen und Kommunikationsstrukturen zum Ideenaustausch und zur Gewinnung von Mitstreiter:innen. Weiterhin bietet die Marke Bits-und-Bäume eine gewisse Reichweite für öffentliche Aufmerksamkeit.

## Was die Gruppe braucht

Damit die Gruppe funktioniert, braucht es neben Infrastuktur (Mailinglisten, Webseite, Soziale Medien, ...) und deren Pflege zu aller erst Menschen. Menschen, die anderen zuhören als Resonanzraum für Ideen zur Verfügung stehen und im Rahmen ihrer Möglichkeiten und Interessen Hilfe anbieten.


## Wie wir in der Gruppe zusammenarbeiten

Wir streben einen konstruktiven und respektvollen Dialog an, sowohl gruppenintern als auch nach außen. Innerhalb der Gruppe versuchen wir allen vorhandenen Meinungen angemessenen Raum zu geben und bei wichtigen Entscheidungen einen Konsens zu erreichen. Zur Verhinderung von ernsten Konflikten bemühen wir uns aktiv Missverständnisse zu vermeiden und frühzeitige deeskalierend zu wirken. Kritik findet stets auf der Sachebene und nicht auf persönlicher Ebene statt.

Mit der Zeit und Aufmerksamkeit aller Beteiligten gehen wir wertschätzend um. Wir versuchen, unsere Möglichkeiten und Grenzen realistisch einzuschätzen und uns nicht zu überfordern.

---

=========================
Vorschlag Präamble (erste unserer Forderungen, könnte auch ein Textblock seitlich daneben sein.
                   Mir ist bewusst, dass wir als lokale Gruppe die Themenvielfalt garnicht komplett abdenken können, wir sollten aber offen für die Themen sein. ) :

     Die Gestaltung der Digitalisierung soll dem Gemeinwohl dienen. Sie darf nicht einseitig auf die
     Förderung einer wirtschafts- und wachstumspolitischen Agenda abzielen, sondern muss auf sozial-,
     umwelt-, entwicklungs- und friedenspolitische Ziele ausgerichtet sein. Die Digitalisierung soll
     zu einer nachhaltigen Energie-, Verkehrs-, Agrar- oder Ressourcenwende beitragen und konkrete Beiträge zur umfassenden Gewährleistung der Menschenrechte,
     der Klimaschutzziele und zur Beendigung von Hunger und Armut leisten. Eine nachhaltige Digitalisierung in unserem Sinne setzt
      auf sinnvolle, menschenwürdige Arbeit, soziale Gerechtigkeit und suffiziente Lebensstile.

Vorschlag einer Erweiterung (Rave):
    Dafür muss die gesamte Gesellschaft ihren Umgang mit IT-Systemen reflektieren. Jedes Voranschreiten in Richtung einer digitalisierten Gesellschaft hat auch ökologische und soziale Folgen, die es zu bedenken gilt.
[Das Folgende ist wahrscheinlich zu lang und zu konkret. Aber ich möchte es trotzdem hier teilen, denn der letzte Satz würde eine gute "aktivistische" Motivation geben]
Es ist nicht zielführend, auf geschlossene ("closed source") Systeme zu setzen, da diese schwerer auf Sicherheit zu prüfen sind, langfristige Verfügbarkeit nicht immer gewährleistet ist, die Abhängigkeit von Softwarekonzernen in vielen Bereichen nicht tragbar ist und die Standardisierung von Schnittstellen durch Wettbewerbsdruck erschwert wird. Kostspielige Lizenzierungsmodelle schaffen eine zusätzliche Barriere für kleine Institutionen und Privatpersonen, an der Digitalisierung sinnvoll teilzunehmen. Wir befürworten daher den Einsatz von freier Software.
Weiterhin ist es zu hinterfragen, inwieweit Digitalisierung den sog. Rebound-Effekt begünstigt. Dieser besagt unter anderem, dass Effizienzgewinne in technischen Lösungen durch verstärkte Nutzung wieder entwertet werden können.
Diese und weitere Problemfelder zeigen, dass die Digitalisierung kritisch begleitet werden muss und weit mehr als die reine technische Entwicklung umfasst.
---

Vorschlag zu weiterem Punkt:
## Wie wir nach außen auftreten

Wir pflegen unseren Internet-Auftritt unter dresden.bits-und-baeume.org, wo wir neben allgemeinen Informationen und Veranstaltungshinweisen auch ein Blog betreiben. Beim Auftritt in sozialen Medien setzen wir vorrangig auf das freie, föderale Netzwerk Mastodon.
Erreichbar für Anfragen aller Art sind wir am besten per Mail unter foo@bar.org. Der Vernetzung und Zusammenarbeit mit anderen aktivistischen Gruppen schenken wir dabei besondere Aufmerksamkeit.
Hilfestellung zu konkreten IT-Problemen können wir nur dann geben, wenn es die Zeit unserer Mitglieder zulässt.

---

Wir arbeiten auch gerne mit profitorientierten Unternehmen zusammen, sofern sie an einem dauerhaften Wandel zu nachhaltigerer Techniknutzung interessiert sind. Kurzfristige Image-Boosts durch die Verwendung von Buzzwords (gemeinhin als Greenwashing bezeichnet) oder unseres Namens lehnen wir ab.



Vorschlag:
## Wie wir in der Gruppe zusammenarbeiten

Wir streben einen konstruktiven und respektvollen Dialog an, sowohl gruppenintern als auch nach außen. Wir sind grundsätzlich für alle Menschen offen. Menschenfeindliche Haltung lehnen wir ab. Innerhalb der Gruppe versuchen wir allen vorhandenen Meinungen angemessenen Raum zu geben und bei wichtigen Entscheidungen einen Konsens zu erreichen. Zur Verhinderung von ernsten Konflikten bemühen wir uns, aktiv Missverständnisse zu vermeiden und frühzeitig deeskalierend zu wirken. Kritik findet stets auf der Sachebene und nicht auf persönlicher Ebene statt.



=====================

Vorschlag Jonas, 17. Juni 2020 19:00 Uhr


In Zeiten ständiger Leistungssteigerungen der Computer war deren absolute Umweltwirkung noch klein. Ressourcenintensive Software ist hauptsächlich durch ihre Langsamkeit oder die laut aufdrehenden Lüfter aufgefallen. Die kommerziellen Programme waren rechenintensiver als die freien Alternativen und spielten in das Geschäftsmodell der Hardwarehersteller, einen noch funktionierenden PC alt aussehen zu lassen, um die Verkäufe von neuen zu stimulieren.

Heute hat sich das Bild gewandelt. Die Prozessoren werden nicht schneller, aber die Hardwareproduktion hat einen großen Ressourcenhunger und der elektronische Stromverbrauch der Netzwerke und Rechner nimmt einen beträchtlichen Anteil der globalen Energie in Anspruch, wobei Filme ein wichtiger Faktor sind und die Frage nach der Rolle der Technikfirmen noch geklärt werden muss. Außerdem treten durch die allgegenwärtige Nutzung des Internets für alle Lebensbereiche neben den positiven Aspekten auch negative soziale Auswirkungen auf.


Neben den alten, aber dennoch aktuellen Fragen:

- Welche Monopole gibt es und wie können sie gebrochen werden?
- Was ist Freiheit von Software und wie kommt man dahin?

Gibt es also neue Fragen:

- Kann der Energie- und Ressourcenverbrauch reduziert werden?
- Werden nur sinnvolle Anwendungen mit den wertvollen Ressourcen betrieben, oder sind darunter Sachen und Leben zerstörende Dinge?
- Welche Benachteiligungen verursacht die Technik und wie können diese aufgehoben werden?
- Wird die Leistungsfähigkeit der Computer möglicherweise stagnieren und wenn ja, wie erzählt man es den Leuten, die heute in massive Machine-Learning-Technologien investieren?




Wir unterstützen die übergeordneten Bits- und Bäume-Forderungen und  fühlen uns in diesem Sinne mit den anderen Bits- und Bäume-Gruppen verbunden. Im Allgemeinen wollen wir Wissen und Einschätzungen aufbauen und auf verschiedenste Weise teilen sowie eine Technikberatung auf indivdueller Ebene leisten und grundsätzlich auch Entscheidungsträgön unsere Meinung erklären, auch wenn dies alles möglicherweise auf mehreren Ebenen vermeintlich gegen Interessen von Wirtschaftsunternehmen zu laufen scheint.

Wir als lokale Gruppe wollen in diesem Sinne sowohl mit der allgemeinen Bewegung zusammenarbeiten als auch lokal tätig sein.





## Wie wir in der Gruppe zusammenarbeiten

Wir streben einen konstruktiven und respektvollen Dialog an, sowohl gruppenintern als auch nach außen. Wir sind grundsätzlich für alle Menschen offen. Menschenfeindliche Haltung lehnen wir ab. Innerhalb der Gruppe versuchen wir allen vorhandenen Meinungen angemessenen Raum zu geben und bei wichtigen Entscheidungen einen Konsens zu erreichen. Zur Verhinderung von ernsten Konflikten bemühen wir uns aktiv Missverständnisse zu vermeiden und frühzeitige deeskalierend zu wirken. Kritik findet stets auf der Sachebene und nicht auf persönlicher Ebene statt.


=====================

Vorschlag 1 als Ersatz für "Thema und Ausrichtung"

## Präambel

Digitalisierung und Nachhaltigkeit sind die existenziellen gesellschaftlichen Herausforderungen des 21. Jahrhunderts. Die Digitalisierung und ihre Folgeprozesse bergen ökologisch und sozial große Chancen aber auch große Risiken. Schon jetzt sind viele gravierende Probleme offenkundig. Diese Prozesse bedürfen daher einer intensiven Begleitung durch eine kritische und wachsame Zivilgesellschaft. Der Dresdner Zweig begreift sich als Teil der überregionalen Bits&Bäume-Bewegung, welche sich für eine nachhaltige Digitalisierung einsetzt. Die inhaltliche Ausrichtung basiert dabei auf den [Forderungen der Bits-und-Bäume-Konferenz 2018](https://bits-und-baeume.org/forderungen/info/de).


=====================

Vorschlag 2 als Ersatz für "Thema und Ausrichtung"

<Vorschlag 1>, d.h. insbesondere auf unveräußerlichen Menschenrechten, Schutz natürlicher Lebensgrundlagen, Ressourcenschonung und sozialer Gerechtigkeit.

=====================

Vorschlag 3 als Ersatz für "Thema und Ausrichtung"


<Vorschlag 1>, d.h. insbesondere auf Menschenrechten, Schutz natürlicher Lebensgrundlagen, Ressourcenschonung, sozialer Gerechtigkeit und suffizienter Lebensstile.


=====================



=== KOPIE ZUR SICHERHEIT ===


GRUNDSTRUKTUR
# Bits und Bäume Dresden Selbstverständnis

NEU
In den Anfangszeiten ständiger Leistungssteigerungen der Computer war deren absolute Umweltwirkung noch klein. Ressourcenintensive Software ist hauptsächlich durch ihre Langsamkeit oder die laut aufdrehenden Lüfter aufgefallen. Die kommerziellen Programme waren rechenintensiver als die freien Alternativen und spielten in das Geschäftsmodell der Hardwarehersteller, einen noch funktionierenden PC alt aussehen zu lassen, um die Verkäufe von neuen zu stimulieren.

NEU
Heute hat sich das Bild gewandelt. Der Energieverbrauch der einzelnen Prozessoren stagniert, aber die Zahl an Computern, Minicomputern und Rechenzentren steigt. Folglich nimmt der Stromverbrauch mittlerweile einen beträchtlichen Anteil der globalen Energienutzung ein. Um den Hardwarenachschub und die Lieferketten sorgen sich die eher direkt-eigennützigen Menschen, während sich die eher indirekt-eigennützigen Menschen wegen der Schädigung von Natur, Umwelt und Klima grämen.

NEU
In dieser Situation stellen wir uns Fragen wie diese:
* Werden sich die Paradigmen der IT ändern, und wenn ja, wie disruptiv?
* Was müssen wir tun, um uns und unsere Mitmenschen auf die Zukunft einzustellen?
* Werden nur sinnvolle Anwendungen mit den wertvollen Ressourcen betrieben, oder sind darunter Techniken, die helfen, besonders effizient Sachen und Leben zu zerstören?
* Welcher Mechanismus steht dahinter, dass sich die IT-Branche häufig als nachhaltig bezeichnet, aber trotzdem steigende Umweltfolgen verursacht?
* Welche Benachteiligungen verursacht die Technik und wie können diese vermieden/kompensiert werden?
* Wie können die Menschen in Würde an der weltweiten globalen Kommunikation teilnehmen.

NEU
Wir glauben nicht, dass die Buzzwords der IT-Branche hinreichend für die Beantwortung dieser auch sozialen Fragen sind. Diesem Ziel kommen wir durch die [Forderungen der Bits-und-Bäume-Konferenz 2018](https://bits-und-baeume.org/forderungen/info/de) viel näher.

NEU
In diesem Sinne fühlen wir uns mit den anderen Bits- und Bäume-Gruppen verbunden.



GRUNDSTRUKTUR
## Ziele

Unsere Ziele sind:

- Die Vernetzung von digitaler Bürger:innenrechte-Bewegung und allgemein technisch interessierten Menschen ("Bits") und umweltorientierter Nachhaltigkeitsbewegung ("Bäume").
- Die Befähigung zivilgesellschaftlicher Akteur:innen und öffentlicher Institutionen zu möglichst nachhaltiger IT-Nutzung.
NEU
- Ermutigung zur Nutzung freier Software.
GRUNDSTRUKTUR
- Öffentlichkeitsarbeit für die Themen und Forderungen der Bits- und Bäume Konferenz.
- Motivation und Befähigung von Individuen zur Umsetzung der Forderungen beizutragen.
NEU
- Systematische Sammlung und Bereitstellung von Wissen der Technik-Nachhaltigkeitsaspekte.
- Eigene Reflektion unserer Techniknutzung.

GRUNDSTRUKTUR
## Was die Gruppe bietet

Bits und Bäume Dresden ist eine Plattform, um eigene thematisch passende Ideen umzusetzen. Dafür bietet die Gruppe eine organisatorischen und sozialen Rahmen, d.h. regelmäßige Treffen und Kommunikationsstrukturen zum Ideenaustausch und zur Gewinnung von Mitstreiter:innen. Weiterhin bietet die Marke Bits-und-Bäume eine gewisse Reichweite für öffentliche Aufmerksamkeit.

## Was die Gruppe braucht
Damit die Gruppe funktioniert, braucht es neben Infrastuktur (Mailinglisten, Webseite, Soziale Medien, ...) und deren Pflege zu allererst Menschen. Menschen, die anderen zuhören als Resonanzraum für Ideen zur Verfügung stehen und im Rahmen ihrer Möglichkeiten und Interessen Hilfe anbieten.

NEU
Du bist also herzlich eingeladen, zu uns zu stoßen und gemeinsam Ziele umzusetzen!

GRUNDSTRUKTUR
## Wie wir in der Gruppe zusammenarbeiten

MODIFIKATION DER GRUNDSTRUKTUR: auch halb-interner Dialog aus Dresdner Sicht
Wir streben einen konstruktiven und respektvollen Dialog an, sowohl lokal gruppenintern als auch mit der allgemeinen Bewegung als auch nach außen.
GRUNDSTRUKTUR
Innerhalb der Gruppe versuchen wir allen vorhandenen Meinungen angemessenen Raum zu geben und bei wichtigen Entscheidungen einen Konsens zu erreichen. Zur Verhinderung von ernsten Konflikten bemühen wir uns aktiv Missverständnisse zu vermeiden und frühzeitige deeskalierend zu wirken. Kritik findet stets auf der Sachebene und nicht auf persönlicher Ebene statt.

GRUNDSTRUKTUR
Mit der Zeit und Aufmerksamkeit aller Beteiligten gehen wir wertschätzend um. Wir versuchen, unsere Möglichkeiten und Grenzen realistisch einzuschätzen und uns nicht zu überfordern.



